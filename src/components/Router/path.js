import React from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Dashboard from '../Dashboard/dashboard'
import Dashboard2 from '../Dashboard2/dashboard2'
const Path=(props)=>(
		<Router>
			<div>
				<Route path="/" exact component={Dashboard} />
				<Route path="/next" component={Dashboard2}/>
			</div>
		</Router>

	)
export default Path