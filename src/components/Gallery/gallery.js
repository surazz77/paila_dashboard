import React from 'react'
import '../../App.css'
import CardImages from '../card/images'
const Gallery=(props)=>(
	<div className='gallery'>
		<div className='galleryTitle'>
			<h1>GALLERY</h1>
		</div>
		<div className='cardimage'>
			<CardImages/>
			<CardImages/>
			<CardImages/>
			<CardImages/>
			<CardImages/>
			<CardImages/>
			<CardImages/>
			<CardImages/>
			<CardImages/>
			<CardImages/>
		</div>
	</div>
	)
export default Gallery