import React from 'react'
import Sidebars from '../Sidebar/sidebar'
import Icon from '../topIcon/icon'
import NavBar from '../topNav/nav'
import Upload from '../Upload/upload'
import Gallery from '../Gallery/gallery'

const Dashboard=(props)=>(
    <div className='Dashboard'>
    	<div>
    		<Sidebars/>
    	</div>
    	<div>
    		<Icon/>
    	</div><br/><br/><br/>
    	<div>
    		<NavBar/>
    	</div>
    	<div>
    		<Upload/>
    	</div>
    	<div>
    		<Gallery/>
    	</div>
       </div>
)
export default Dashboard