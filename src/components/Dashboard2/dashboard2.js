import React from 'react'
import Sidebars from '../Sidebar/sidebar'
import Icon from '../topIcon/icon'
import NavBar from '../topNav/nav'
import Content from '../Content/content'
import Gallery2 from '../Gallery2/gallery2'
import Chartcontent from '../Chart-Content/chart-content'
import '../../App.css';
const Dashboard2=(props)=>(
	<div className='Dashboard'>
    	<div>
    		<Sidebars/>
    	</div>
    	<div>
    		<Icon/>
    	</div><br/><br/><br/>
    	<div>
    		<NavBar/>
    	</div>
        <div>
            <Content/>
        </div>
        <div>
            <Chartcontent/>
        </div>
        <div>
            <Gallery2/>
        </div>
    </div>

)
export default Dashboard2