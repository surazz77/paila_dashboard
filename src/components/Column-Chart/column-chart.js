import React ,{Component} from 'react'
import {Bar } from 'react-chartjs-2'
class ColumnChart extends Component {
	state={
		chartdata:{
			labels:['nepal','india','china','bhutan','Australia','japan','usa'],
			datasets:[{
				data:[
					500,800,700,900,1000,600,450
					],
					backgroundColor:[
				'rgba(255, 0, 0, 0.2)',
				'rgba(255, 0, 0, 0.4)',
				'rgba(255, 0, 0, 0.6)',
				'rgba(255, 0, 0, 0.8)',
				'rgba(155, 0, 0, 0.8)',
				'rgba(100, 0, 0, 0.2)',
				'rgba(350, 0, 0, 0.8)',

			]
			}]
		
	
		}
	}
	render() {
			
		return (
		<div>
			<Bar
				data={this.state.chartdata}
				width={100}
				height={20}
				options={{

					legend:{
						display:false
					},
					 scales:{
					 	yAxes: [{
                			display: false, 
                			barPercentage: 0.8
            }],
            		 	xAxes: [{ 
                			barPercentage: 1.0,
    						categoryPercentage: 0.5,
    						categorySpacing: 0
            }],
        	
					 }
				}}
			/>

		</div>
		);
	}
}
export default ColumnChart;                              