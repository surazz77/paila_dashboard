import React, { Component } from 'react'
import { Button, Header, Icon, Image, Menu, Segment, Sidebar } from 'semantic-ui-react'
import '../../App.css';
class Sidebars extends Component {
	constructor(props) {
		super(props)

		this.state = {
			showSideBar: false,
		}
	}

	toggleSidebar = () => {
		this.setState(({showSideBar}) => ({
			showSideBar: !showSideBar
		}))
	}

  render() {
  	const {showSideBar} = this.state
  	console.log(showSideBar)

    return (
    	<div>
    		
    		{showSideBar ? 
    			<div className='sidebar'>
    				<i className="fa fa-bars bar" onClick={this.toggleSidebar}></i>
					<ul>
						<div>Logo</div>
						<li><a>General</a></li>
						<li><span className='iconBorder'><img src="https://png.icons8.com/color/50/000000/home.png" className='iconImage'/></span><button className = 'sideButton1'>HOME</button></li>
						<li><span className='iconBorder'><img src="https://png.icons8.com/color/50/000000/home.png" className='iconImage'/></span><button className = 'sideButton1'>FORMS</button></li>
						<li><span className='iconBorder'><img src="https://png.icons8.com/color/50/000000/home.png" className='iconImage'/></span><button className = 'sideButton1'>ELECTRONICS</button></li>
						<li><span className='iconBorder'><img src="https://png.icons8.com/color/50/000000/home.png" className='iconImage'/></span><button className = 'sideButton1'>Tables</button></li>
						<li><span className='iconBorder'><img src="https://png.icons8.com/color/50/000000/home.png" className='iconImage'/></span><button className = 'sideButton1'>DATA</button></li>
						<li><span className='iconBorder'><img src="https://png.icons8.com/color/50/000000/home.png" className='iconImage'/></span><button className = 'sideButton1'>LAYOUTS</button></li>
					</ul>
				</div> : <i className="fa fa-bars" onClick={this.toggleSidebar}></i>
    		}
			
    	</div>
    )
  }
}

export default Sidebars